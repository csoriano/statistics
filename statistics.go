package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"os/user"
	"sort"
	"strings"
	"time"

	gitlab "github.com/xanzy/go-gitlab"
)

const startDate = "2018-10-01" // ISOTime format
const endDate = "2019-09-30"   // ISOTime format

const relativeTokenPath = "/.local/share/statistics/gitlabToken.txt" // Relative to $HOME

const baseURL = "https://gitlab.gnome.org/api/v4"

type Statistic struct {
	Name           string
	MRsMerged      MRsMergedCount
	MRsOpened      MRsOpenedCount
	MRsComments    MRsCommentsCount
	IssuesClosed   IssuesClosedCount
	IssuesOpened   IssuesOpenedCount
	IssuesComments IssuesCommentsCount
	Events         EventsCount
}

func main() {
	fromDisk := flag.Bool("disk", false, "Read user data from disk. Format and naming should be as processed by this program")

	flag.Parse()

	var data []Statistic
	if *fromDisk {
		data = retrieveFromDisk()
	} else {
		data = retrieveRemote()
	}

	// Closures that order the Statistic structure.
	MRsMergedSort := func(s1, s2 *Statistic) bool {
		return s1.MRsMerged > s2.MRsMerged
	}

	MRsOpenedSort := func(s1, s2 *Statistic) bool {
		return s1.MRsOpened > s2.MRsOpened
	}

	MRsCommentsSort := func(s1, s2 *Statistic) bool {
		return s1.MRsComments > s2.MRsComments
	}

	issuesClosedSort := func(s1, s2 *Statistic) bool {
		return s1.IssuesClosed > s2.IssuesClosed
	}

	issuesOpenedSort := func(s1, s2 *Statistic) bool {
		return s1.IssuesOpened > s2.IssuesOpened
	}

	issuesCommentsSort := func(s1, s2 *Statistic) bool {
		return s1.IssuesComments > s2.IssuesComments
	}

	chatty := func(s1, s2 *Statistic) bool {
		return int(s1.IssuesComments)+int(s1.MRsComments) > int(s2.IssuesComments)+int(s2.MRsComments)
	}

	var err error
	By(MRsMergedSort).Sort(data)
	MRsMergedJSON, _ := json.MarshalIndent(data, "", "    ")
	err = ioutil.WriteFile("MRsMerged.json", MRsMergedJSON, 0644)
	check(err)

	By(MRsOpenedSort).Sort(data)
	MRsOpenedJSON, _ := json.MarshalIndent(data, "", "    ")
	err = ioutil.WriteFile("MRsOpened.json", MRsOpenedJSON, 0644)
	check(err)

	By(MRsCommentsSort).Sort(data)
	MRsCommentsJSON, _ := json.MarshalIndent(data, "", "    ")
	err = ioutil.WriteFile("MRsComments.json", MRsCommentsJSON, 0644)
	check(err)

	By(issuesClosedSort).Sort(data)
	issuesClosedJSON, _ := json.MarshalIndent(data, "", "    ")
	err = ioutil.WriteFile("issuesClosed.json", issuesClosedJSON, 0644)
	check(err)

	By(issuesOpenedSort).Sort(data)
	issuesOpenedJSON, _ := json.MarshalIndent(data, "", "    ")
	err = ioutil.WriteFile("issuesOpened.json", issuesOpenedJSON, 0644)
	check(err)

	By(issuesCommentsSort).Sort(data)
	issuesCommentsJSON, _ := json.MarshalIndent(data, "", "    ")
	err = ioutil.WriteFile("issuesComments.json", issuesCommentsJSON, 0644)
	check(err)

	By(chatty).Sort(data)
	chattyJSON, _ := json.MarshalIndent(data, "", "    ")
	err = ioutil.WriteFile("chatty.json", chattyJSON, 0644)
	check(err)
}

func retrieveFromDisk() []Statistic {
	b, err := ioutil.ReadFile("data.json") // just pass the file name
	check(err)
	str := string(b) // convert content to a 'string'

	var data []Statistic
	err = json.Unmarshal([]byte(str), &data)

	return data
}

func retrieveRemote() []Statistic {
	usr, err := user.Current()
	check(err)
	b, err := ioutil.ReadFile(usr.HomeDir + relativeTokenPath)
	check(err)
	token := strings.TrimSpace(string(b))
	git := gitlab.NewClient(nil, token)
	git.SetBaseURL(baseURL)

	matchingGroups, _, err := git.Groups.SearchGroup("GNOME", nil)
	check(err)
	if len(matchingGroups) != 1 {
		panic("More than one matching group with name GNOME")
	}
	gnomeGroup := matchingGroups[0]
	opts := gitlab.ListGroupMembersOptions{}
	opts.PerPage = 100
	_, res, err := git.Groups.ListAllGroupMembers(gnomeGroup.ID, &opts, nil)
	check(err)
	totalMembers := res.TotalItems
	totalPages := res.TotalPages
	fmt.Println("GNOME group has", totalMembers, "members")

	var data []Statistic
	for page := 0; page < totalPages; page++ {
		opts.Page = page
		members, _, err := git.Groups.ListAllGroupMembers(gnomeGroup.ID, &opts, nil)
		check(err)
		for i, member := range members {
			fmt.Println("Processing member", (i+1)*(page+1), "of", totalMembers)
			fmt.Println(member.Name, member.ID)
			statistic := fetchFromGNOME(git, member.ID)
			statistic.Name = member.Name
			fmt.Println("")
			fmt.Println("Last year activity:", statistic.Name)
			fmt.Println("-------------------------------------")
			fmt.Println("MRs merged:", statistic.MRsMerged)
			fmt.Println("MRs opened:", statistic.MRsOpened)
			fmt.Println("MRs comments:", statistic.MRsComments)
			fmt.Println("Issues closed:", statistic.IssuesClosed)
			fmt.Println("Issues opened:", statistic.IssuesOpened)
			fmt.Println("Issues comments:", statistic.IssuesComments)
			fmt.Println("Total events (including others):", statistic.Events)
			fmt.Println("")
			data = append(data, statistic)
		}
	}
	return data
}

// By is the type of a "less" function that defines the ordering of its Planet arguments.
type By func(s1, s2 *Statistic) bool

// statisticSorter joins a By function and a slice of Statistics to be sorted.
type statisticsSorter struct {
	statistics []Statistic
	by         func(s1, s2 *Statistic) bool // Closure used in the Less method.
}

// Len is part of sort.Interface.
func (s *statisticsSorter) Len() int {
	return len(s.statistics)
}

// Swap is part of sort.Interface.
func (s *statisticsSorter) Swap(i, j int) {
	s.statistics[i], s.statistics[j] = s.statistics[j], s.statistics[i]
}

// Less is part of sort.Interface. It is implemented by calling the "by" closure in the sorter.
func (s *statisticsSorter) Less(i, j int) bool {
	return s.by(&s.statistics[i], &s.statistics[j])
}

func (by By) Sort(statistics []Statistic) {
	ss := &statisticsSorter{
		statistics: statistics,
		by:         by, // The Sort method's receiver is the function (closure) that defines the sort order.
	}
	sort.Sort(ss)
}

type MRsMergedCount int
type MRsOpenedCount int
type MRsCommentsCount int
type IssuesClosedCount int
type IssuesOpenedCount int
type IssuesCommentsCount int
type EventsCount int

func fetchFromGNOME(git *gitlab.Client, userID int) Statistic {
	// Just checking format is ok
	DateOne, err := time.Parse("2006-01-02", startDate)
	check(err)
	DateTwo, err := time.Parse("2006-01-02", endDate)
	check(err)
	opts := gitlab.ListContributionEventsOptions{}
	// golang cannot take address or create an address from static strings,
	// so need to put into a var first...
	tmpDate := gitlab.ISOTime(DateOne)
	opts.After = &tmpDate 
	tmpDate2 := gitlab.ISOTime(DateTwo)
	opts.Before = &tmpDate2
	opts.PerPage = 100
	_, res, err := git.Users.ListUserContributionEvents(userID, &opts)
	check(err)
	totalPages := res.TotalPages
	totalEvents := EventsCount(res.TotalItems)
	var mrsMerged MRsMergedCount
	var mrsOpened MRsOpenedCount
	var mrsComments MRsCommentsCount
	var issuesClosed IssuesClosedCount
	var issuesOpened IssuesOpenedCount
	var issuesComments IssuesCommentsCount
	for page := 0; page < totalPages; page++ {
		opts.Page = page
		fmt.Println("Processing contributions", (page+1)*100, "of", totalEvents)
		contributions, _, err := git.Users.ListUserContributionEvents(userID, &opts)
		check(err)
		for _, cont := range contributions {
			if cont.TargetType == "MergeRequest" {
				if cont.ActionName == "closed" || cont.ActionName == "accepted" {
					mrsMerged++
				} else if cont.ActionName == "opened" {
					mrsOpened++
				} else {
					fmt.Println("#########################")
					fmt.Println("Unhandled MR")
					fmt.Println("#########################")
					fmt.Println(cont)
					fmt.Println("~~~~~~~~~~~~~~~~~~~~~~~~~")
				}
			} else if cont.TargetType == "Issue" {
				if cont.ActionName == "closed" {
					issuesClosed++
				} else if cont.ActionName == "opened" {
					issuesOpened++
				} else {
					fmt.Println("#########################")
					fmt.Println("Unhandled Issue")
					fmt.Println("#########################")
					fmt.Println(cont)
					fmt.Println("~~~~~~~~~~~~~~~~~~~~~~~~~")
				}
			} else if cont.TargetType == "Note" || cont.TargetType == "DiscussionNote" || cont.TargetType == "DiffNote" {
				if cont.ActionName == "commented on" {
					if cont.Note.NoteableType == "Commit" {
						// Notes on commits themselves, usually for releases
						continue
					}

					if cont.Note.NoteableType == "Snippet" {
						// Just snippets
						continue
					}

					if cont.Note.NoteableType == "MergeRequest" {
						mrsComments++
					} else if cont.Note.NoteableType == "Issue" {
						issuesComments++
					} else {
						fmt.Println("#########################")
						fmt.Println("Unhandled", cont.TargetType, "Comment")
						fmt.Println("#########################")
						fmt.Println(cont)
						fmt.Println("~~~~~~~~~~~~~~~~~~~~~~~~~")
					}
				} else {
					fmt.Println("#########################")
					fmt.Println("Unhandled Note")
					fmt.Println("#########################")
					fmt.Println(cont)
					fmt.Println("~~~~~~~~~~~~~~~~~~~~~~~~~")
				}
			} else {
				if cont.TargetType == "" {
					if cont.ActionName == "pushed to" || cont.ActionName == "deleted" ||
						cont.ActionName == "pushed new" {
						// It's a pushed or deleted branch event
						continue

					} else if cont.ActionName == "joined" || cont.ActionName == "left" {
						// Joining and leaving projects or groups
						continue
					} else if cont.ActionName == "created" || cont.ActionName == "imported" {
						// Creating projects or groups
						continue
					}
				}

				if cont.TargetType == "Milestone" {
					continue
				}

				fmt.Println("#########################")
				fmt.Println("Unhandled type")
				fmt.Println("#########################")
				fmt.Println(cont.TargetType)
				fmt.Println(cont.ActionName)
				fmt.Println(cont)
				fmt.Println("~~~~~~~~~~~~~~~~~~~~~~~~~")
			}
		}
	}
	return Statistic{"", mrsMerged, mrsOpened, mrsComments, issuesClosed, issuesOpened, issuesComments, totalEvents}
}

func check(e error) {
	if e != nil {
		panic(e)
	}
}

func checkOk(ok bool) {
	if !ok {
		panic(new(error))
	}
}
